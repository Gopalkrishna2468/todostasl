import { TodosService } from './../../../services/todos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  text: string = ''
  constructor(private todoService: TodosService) {
    this.todoService.todos$.subscribe((todos) => {
      console.log('todos', todos)
    })
  }

  ngOnInit(): void {
  }
  changeText(event: Event) {
    const target = event.target as HTMLInputElement;
    this.text = target.value
    console.log(target.value)
  }
  addTodo() {

    this.todoService.addTodo(this.text)
    this.text = ''
  }
}
