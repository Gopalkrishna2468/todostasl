import { data } from './../../types/filter.enum';
import { map } from 'rxjs/operators';
import { TodosService } from './../../../services/todos.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  'noTodosClass$': Observable<boolean>;
  'activeCount$': Observable<number>;
  'itemsLeftText$': Observable<string>;
  'filterEnum': any = data;
  'filter$': Observable<any>;



  constructor(private todosService: TodosService) {


  }

  ngOnInit(): void {
    console.log(this.filterEnum);

    if (this.filterEnum == null || this.filterEnum == undefined) {

    }
    console.log(typeof (this.filterEnum))
    console.log(this.filterEnum)
    this.activeCount$ = this.todosService.todos$.pipe(map(todos => todos.filter(todo => !todo.isCompleted).length))
    this.itemsLeftText$ = this.activeCount$.pipe(
      map(activeCount => `TasksLeft${activeCount !== 1 ? 's' : ''}`)
    )

    this.noTodosClass$ = this.todosService.todos$.pipe(map(todos => todos.length === 0))

    this.filter$ = this.todosService.filter$;

  }
  changeFilter(event: Event, filter: any) {
    event.preventDefault();
    this.todosService.changeFilter(filter)
    console.log(filter)
  }



}
