import { data } from './../../types/filter.enum';
import { TodosService } from './../../../services/todos.service';
import { TodoInterFace } from './../../types/todo.interface';
import { Component, OnInit } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  visibleTodos$: Observable<TodoInterFace[]>;
  noTodoClass$: Observable<boolean>
  isAllTodosSelected$: Observable<boolean>
  Data: any = data
  allCompleted: boolean = true
  allCompletedd: boolean = true

  constructor(private todosService: TodosService) {
    this.isAllTodosSelected$ = this.todosService.todos$.pipe(map(todos => todos.every(todo => todo.isCompleted)));

    this.noTodoClass$ = this.todosService.todos$.pipe(map(todos => todos.length === 0));

    this.visibleTodos$ = combineLatest(
      this.todosService.todos$,
      this.todosService.filter$
    ).pipe(
      map(([todos, filter]: [TodoInterFace[], any]) => {
        if (filter === this.Data.active) {
          return todos.filter(todo => !todo.isCompleted);
        } else if (filter === this.Data.completed) {
          return todos.filter((todo) => todo.isCompleted);
        }
        return todos;
      }))

  }

  ngOnInit(): void {
  }
  toggleAllTodos(event: Event) {
    const target = event.target as HTMLInputElement
    this.todosService.toggleAll(target.checked)
  }
  removeAllTodo(event: any) {
    console.log('removeTodo')
    if (this.isAllTodosSelected$) {

      this.todosService.removeAllTodo()
      this.allCompleted = !this.allCompleted
      const div = event.target.parentElement
      console.log(div);
      div.style.display = "none"

    }
    return
  }
}
