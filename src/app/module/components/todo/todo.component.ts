import { TodosService } from './../../../services/todos.service';
import { TodoInterFace } from './../../types/todo.interface';
import { Component, Input, OnInit } from '@angular/core';
import { Action } from 'rxjs/internal/scheduler/Action';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input('todo') todoprops!: TodoInterFace
  selected: any = true

  constructor(private todoService: TodosService) { }

  ngOnInit(): void {
  }
  removeTodo() {
    console.log('removeTodo')

    this.todoService.removeTodo(this.todoprops.id)


  }
  toggleTodo() {
    console.log('toggleTodo')
    this.todoService.toggleTodo(this.todoprops.id)
  }
}
