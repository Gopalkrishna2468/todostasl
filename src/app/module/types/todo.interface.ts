export interface TodoInterFace {
    id: string;
    text: string;
    isCompleted: boolean
}