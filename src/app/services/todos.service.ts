import { data } from './../module/types/filter.enum';
import { TodoInterFace } from './../module/types/todo.interface';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  todos$ = new BehaviorSubject<TodoInterFace[]>([])
  filter$ = new BehaviorSubject<any>(data.all)

  constructor() { }
  addTodo(text: string) {
    let newTodo: TodoInterFace = {
      text,
      isCompleted: false,
      id: Math.random().toString(12),
    };
    let updatedTodos = [...this.todos$.getValue(), newTodo];
    this.todos$.next(updatedTodos);
  }

  toggleAll(isCompleted: boolean) {
    const updatedTodos = this.todos$.getValue().map(todo => {
      return {
        ...todo,
        isCompleted
      }
    })
    this.todos$.next(updatedTodos)
    console.log('updatedt', updatedTodos)
  }

  removeAllTodo() {
    const deltedTodos = this.todos$.getValue().map(todo => {
      return {
        ...todo,
        id: !todo.id
      }
    })
    console.log('updatedt', deltedTodos)
  }


  changeFilter(filterName: any) {
    this.filter$.next(filterName)
  }
  removeTodo(id: string) {
    const updatedTodos = this.todos$.getValue().filter(todo => todo.id !== id)
    this.todos$.next(updatedTodos)
  }

  toggleTodo(id: any) {
    const updatedTodos = this.todos$.getValue().map(todo => {
      if (todo.id === id) {
        return {
          ...todo,
          isCompleted: !todo.isCompleted,
        }
      }
      return todo;
    })
    this.todos$.next(updatedTodos)
  }
}
